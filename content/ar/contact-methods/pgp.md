---
layout: page
title: PGP
author: mfc
language: ar
summary: وسائل الاتصال
date: 2018-09
permalink: /ar/contact-methods/pgp.md
parent: /ar/
published: true
---

برمجيّة PGP و&nbsp;نظيرتها الحُرَّة GPG تتيح كلٌّ منهما لمِنْ يستخدمها حفظ سرِّيَّة فحوى المراسلات البريدية بتعميتها، لمنع مُقدِّم خدمة البريد الإلكتروني من الاطلاع عليها، أو أيَّ طرف آخر لديه نفاذ إلى المراسلات. لكنْ مع ذلك تبقى واقعة إرسالك رسالة إلى الشّخص أو المنظّمة معلومة للحكومة أو جهات إنفاذ القانون. للحَوْل دون ذلك يمكنك إنشاء حساب بريد بعنوان لا يدلُّ على هويتك؛ باسم مستعار مثلا.

موارد: [Resource on creating alternative email addresses](),[Encrypting Email with Mailvelope: A Beginner's Guide](https://freedom.press/training/encrypting-email-mailvelope-beginners-guide/)
