---
layout: sidebar.pug
title: "أصابك الإنهاك؟"
author: FP
language: ar
summary: "التحرُّش عبر الإنترنت و&nbsp;التهديد و&nbsp;غيرهما من هجمات عبر وسائل الاتّصال يمكنها إثقالنا بالمشاعر و&nbsp;زيادة حملنا العاطفي: فقد نشعر بالذّنب أو الخزي أو القلق أو الغضب أو الحيرة أو قلّة الحيلة، كما قد نخشى على سلامتنا السيكولوجية أو الجسدية."
date: 2018-09-05
permalink: /ar/self-care/
parent: Home
sidebar: >
  <h3>الموارد التالية تحوي المزيد عن كيفية حماية نفسك مِنْْ المشاعر الطاغية:</h3>

  <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Self-Care for Activists: Sustaining Your Most Valuable Resource</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Caring for yourself so you can keep defending human rights</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Self-Care for People Experiencing Harassment</a></li>
    <li><a href="https://www.fightcyberstalking.org/emotional-support">Cyberstalking Victims Emotional Support</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Cyberwomen self care training modulr</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/wellness">Wellness and Community</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Twenty ways to help someone who's being bullied online</a></li>
    <li><a href="https://www.hrresilience.org/">Human Rights Resilience Project</a></li>
  </ul>
---

# أصابك الإنهاك؟


التحرُّش عبر الإنترنت و&nbsp;التهديد و&nbsp;غيرهما مِنْ هجمات عبر وسائل الاتّصال قد تثقل شعورنا و&nbsp;تزيد عبئنا العاطفي: فقد نشعر بالذّنْب أو الخزي أو القلق أو الغضب أو الحيرة أو قلَّة الحيلة، كما قد نخشى على سلامتنا السيكولوجيّة أو الجسديّة.

لا يوجد شعور واحد صحيح في مثل تلك الظروف. فالقابلية للتأثُّر و&nbsp;فداحة ما تعنيه البيانات الشخصية تتفاوتان مِنْ شخصٍ لآخر. كلُّ المشاعر حقَّة، و&nbsp;يجب ألّا تشغل نفسك بما إذا كان ردُّ فعلك صائبًا.

ما ينبغي تذكُّره دومًا أنَّ ما أصابك ليس ذنبك و&nbsp;يجب ألّا تلقي باللائمة على نفسك، و&nbsp;تواصلي مع شخص تثقين فيه يستطيع دعمك في مواجهة الأزمة.

تدارُك عواقب الهجمات عبر الإنترنت تلزمه معلومات عمَّا حدث، لكنَّ جمعها لا يجب أنْ يقع على عاتقك أنت وحدك. فإذا كان لديك شخص تثقين به فاطلبي المساعدة مِنْه و&nbsp;اتِّباع الإرشادات في هذا الموقع، أو مكنِّيها مِنْ النفاذ إلى أجهزتك و&nbsp;حساباتك لجمع المعلومات لك.

