---
layout: page
title: "لا أستطيع النفاذ إلى حسابي"
author: RaReNet
language: ar
summary: "هل تواجهين صعوبة في النفاذ إلى حساب بريدك أو&nbsp;حسابك على منصّة تواصل اجتماعي أو&nbsp;موقع ما على الوِب؟ هل يبدي الحساب نشاطًا لم تقم أنت به؟ توجد حلول لتدارك هذه المشكلة."
date: 2015-08
permalink: /ar/topics/account-access-issues/
parent: /ar/
---

# لا أستطيع النفاذ إلى حسابي

يستعمل أفراد المجتمع المدني حسابات التواصل الاجتماعي بكثافة لنشر المعلومات و&nbsp;لمناصرة قضاياهم، و&nbsp;هذا يؤدي إلى استهداف حساباتهم مِنْ قِبَل الفاعلين المُعادين الذين يسعون إلى اختراق تلك الحسابات، ممّا يضرُّ أفراد المجتمع المدني و&nbsp;الذين يتواصلون معهم.

الغرض مِنْ هذا الدليل مساعدتك في حال ما تعذََّر عليك النفاذ إلى أحد حساباتك بسبب اختراقه.

المسار التالي الغرض مِنْه تحديد طبيعة المشكلة و&nbsp;إيجاد الحلول المحتملة.

## Workflow

### Password_Typo

> أحيانا يتعذَّر علينا الولوج إلى حسابنا لأنَّنا نُدخِل كلمة سرٍّ خاطئة دون أنْ نعي، أو&nbsp;لأنّ لغة لوحة المفاتيح مضبوطة على غير ما نستعمله عادة أو&nbsp;لأنّ زرّ الأحرف اللاتينيّة الكبيرة CAPS مضغوط.
>
> جرِّبي كتابة اسم المستخدم و&nbsp;كلمة السرِّ في مُحرِّر نصوص صافية (plaintext) ثم انسخيها ثم ألصقيها في استمارة الولوج. و&nbsp;تحقّق أيضًا مِنْ إعدادات لغة لوحة المفاتيح، و&nbsp;إذا ما كان زرُّ CAPS مضغوطا.

هل حلَّ أيُّ المقترحات السالفة المشكلة و&nbsp;تمكَّنت مِنْ الولوج إلى حسابك؟

- [نعم](#resolved_end)
- [لا](#What_Type_of_Account_or_Service)

### أيّ*حساب*أو_خدمة

أيُّ حساب أو&nbsp;خدمة يتعذَّر الولوج إليها؟

- [فيسبوك](#Facebook)
- [صفحة فيسبوك](#Facebook_Page)
- [تويتر](#Twitter)
- [گُوگِل\جيميل](#Google)
- [ياهو](#Yahoo)
- [هُتميل\أوْتلُك\لايڤ](#Hotmail)
- [پروتونميل](#ProtonMail)
- [إنستَگرام](#Instagram)

### Facebook_Page

هل للصفحة مديرون آخرون؟

- [نعم](#Other_admins_exist)
- [لا](#Facebook_Page_recovery_form)

### Other_admins_exist

هل يواجه المديرون الآخرون نفس المشكلة؟

- [نعم](#Facebook_Page_recovery_form)
- [لا](#Other_admin_can_help)

### Other_admin_can_help

> اطلب مِنْ مدير آخر إعطاءك صلاحيات الإدارة مجدَّدًا.

هل يحلُّ هذا المشكلة؟

- [نعم](#Fb_Page_end)
- [لا](#account_end)

### Facebook_Page_recovery_form

> عاود الولوج إلى فيسبوك و&nbsp;استعملي [استمارة الإبلاغ عن مشاكل الصفحات](https://www.facebook.com/help/contact/164405897002583).
>
> اعلمي أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاستعادة؟

- [نعم](#resolved_end)
- [لا](#account_end)

### Google

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

- [نعم](#I_have_access_to_recovery_email_google)
- [لا](#Recovery_Form_google)

### I_have_access_to_recovery_email_google

هل تلقّيت مؤخرًا تنويهًا حرجًا بشأن خصوصيّة حساب گُوگِل المربوط؟

- [نعم](#Email_received_google)
- [لا](#Recovery_Form_google)

### Email_received_google

هل في الرسالة رابط لاسترداد حسابك؟

- [نعم](#Recovery_Link_Found_google)
- [لا](#Recovery_Form_google)

### Recovery_Link_Found_google

> اتبع رابط استرداد حسابك.

هل تمكّنت مِنْ استرداد الحساب؟

- [نعم](#resolved_end)
- [لا](#Recovery_Form_google)

### Recovery_Form_google

> جرب [استمارة استرداد الحسابات](https://support.google.com/accounts/answer/7682439?hl=ar)
>
> اعلمي أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمُعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

### Yahoo

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

- [نعم](#I_have_access_to_recovery_email_yahoo)
- [لا](#Recovery_Form_Yahoo)

### I_have_access_to_recovery_email_yahoo

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حساب ياهو؟

- [نعم](#Email_received_yahoo)
- [لا](#Recovery_Form_Yahoo)

### Email_received_yahoo

هل في الرسالة رابط لاسترداد حسابك؟

- [نعم](#Recovery_Link_Found_Yahoo)
- [لا](#Recovery_Form_Yahoo)

### Recovery_Link_Found_Yahoo

> اتبع رابط استرداد حسابك.

هل تمكَّنت مِنْ استرداد الحساب؟

- [نعم](#resolved_end)
- [لا](#Recovery_Form_Yahoo)

### Recovery_Form_Yahoo

> جرّبي [استمارة استرداد الحسابات](https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html).
>
> اعلم أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

### Twitter

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

- [نعم](#I_have_access_to_recovery_email_Twitter)
- [لا](#Recovery_Form_Twitter)

### I_have_access_to_recovery_email_Twitter

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حساب تويتر؟

- [نعم](#Email_received_Twitter)
- [لا](#Recovery_Form_Twitter)

### Email_received_Twitter

هل في الرسالة رابط لاسترداد حسابك؟

- [نعم](#Recovery_Link_Found_Twitter)
- [لا](#Recovery_Form_Twitter)

### Recovery_Link_Found_Twitter

> اتبع رابط استرداد حسابك.

هل تمكَّنت مِنْ استرداد الحساب؟

- [نعم](#resolved_end)
- [لا](#Recovery_Form_Twitter)

### Recovery_Form_Twitter

> جرِّبي [استمارة استرداد الحسابات](https://help.twitter.com/forms/restore)
>
> اعلم أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

### ProtonMail

> جرّب [استمارة تصفير كلمة السرّ](https://protonmail.com/support/knowledge-base/reset-password)
> اعلمي أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

### Hotmail

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

- [نعم](#I_have_access_to_recovery_email_Hotmail)
- [لا](#Recovery_Form_Hotmail)

### I_have_access_to_recovery_email_Hotmail

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حساب هُتميل؟

- [نعم](#Email_received_Hotmail)
- [لا](#Recovery_Form_Hotmail)

### Email_received_Hotmail

هل في الرسالة رابط لتصفير كلمة السرّ؟

- [نعم](#Recovery_Link_Found_Hotmail)
- [لا](#Recovery_Form_Hotmail)

### Recovery_Link_Found_Hotmail

> اتبع رابط تصفير كلمة السرّ.

هل تمكَّنت مِنْ استرداد الحساب؟

- [نعم](#resolved_end)
- [لا](#Recovery_Form_Hotmail)

### Recovery_Form_Hotmail

> جرِّبي [استمارة استرداد الحسابات](https://account.live.com/acsr)
>
> اعلم أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

### Facebook

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

- [نعم](#I_have_access_to_recovery_email_Facebook)
- [لا](#Recovery_Form_Facebook)

### I_have_access_to_recovery_email_Facebook

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حساب فيسبوك؟

- [نعم](#Email_received_Facebook)
- [لا](#Recovery_Form_Facebook)

### تلقيت*رسالة*فيسبوك

هل في الرسالة رابط بما معناه "إذا لم تكن قد طلبت هذا التغيير فأمّن حسابك"؟

- [نعم](#Recovery_Link_Found_Facebook)
- [لا](#Recovery_Form_Facebook)

### Recovery_Link_Found_Facebook

> اتبع رابط تأمين الحساب.

هل تمكَّنت مِنْ استرداد الحساب؟

- [نعم](#resolved_end)
- [لا](#Recovery_Form_Facebook)

### Recovery_Form_Facebook

> جرِّبي [استمارة استرداد الحسابات](https://www.facebook.com/login/identify)
>
> اعلم أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

### Instagram

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيّنين للاسترداد؟

- [نعم](#I_have_access_to_recovery_email_Instagram)
- [لا](#Recovery_Form_Instagram)

### I_have_access_to_recovery_email_Instagram

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حساب إنستگرام؟

- [نعم](#Email_received_Instagram)
- [لا](#Recovery_Form_Instagram)

### Email_received_Instagram

هل في الرسالة رابط لاسترداد حسابك؟

- [نعم](#Recovery_Link_Found_Instagram)
- [لا](#Recovery_Form_Instagram)

### Recovery_Link_Found_Instagram

> اتبع رابط استرداد حسابك.

هل تمكَّنت مِنْ استرداد الحساب؟

- [نعم](#resolved_end)
- [لا](#Recovery_Form_Instagram)

### Recovery_Form_Instagram

> جرِّبي [استمارة استرداد الحسابات](https://help.instagram.com/149494825257596)
>
> اعلم أنَّ الاستجابة لبلاغك قد تكون بعد عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

- [نعم](#resolved_end)
- [لا](#account_end)

### Fb_Page_end

يسعدنا كثيرا أن مشكلتك انحلّت. طالع التوصيات التالية لتقليل احتمال فقدناك النفاذ إلى حسابك في المستقبل.

- فعّل الاستيثاق بمعاملين (2FA) لكل مديري الصفحة
- لا تسند صلاحيات الإدارة إلا للذين تثق فيهم و&nbsp;يردون عند الحاجة

### account_end

إذا لم يفلح الإجراء الموصوف هنا في استرداد حسابك فيمكنك التواصل مع المنظَّمات التالية لطلب العون:

:[](organisations?services=account)

### resolved_end

نرجو أنَّ دليل عدَّة الإسعاف اﻷولي الرقمي هذا قد أفادكم، و&nbsp;نحبُّ معرفة رأيكم [بالبريد الإلكتروني](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

طالعي التوصيات التالية لتقليل احتمال فقدانك النفاذ إلى حسابك في المستقبل.

- مِنْ المفيد دوما تفعيل الاستيثاق بمعاملين (2FA) في كلّ الحسابات التي تدعم هذه الوظيفة.
- لا تضع أبدا كلمة السّر نفسها لأكثر مِنْ حساب. و&nbsp;إذا كنت قد فعلت ذلك بالفعل فغيّريها في أقرب فرصة واضعة كلمة سرّ فريدة في كلّ حساب. حتى ما إذا تسرّبت معلومات مِنْ إحدى الخدمات لَمْ يستطع أحد استعمالها للنفاذ إلى حساباتك الأخرى.
- استعمال مدير لكلمات السرّ سيعينك على وضع كلمات سرّ فريدة قوية لكلّ حساباتك دون الاعتماد على ذاكرتك
- احتَط عند الاتصال عبر شبكات وايفاي عمومية، و&nbsp;يستحسن استعمال VPN في هذه الحالة.

#### موارد

- [عدة الأمان - إنشاء كلمات سرّ قوية و&nbsp;حفظها](https://securityinabox.org/ar/guide/passwords/)
- [الدفاع عن النفس ضد المراقبة - حماية نفسك على مواقع التواصل الاجتماعي](https://ssd.eff.org/ar/module/حماية-نفسك-على-مواقع-التواصل-الاجتماعي)

<!--- Edit the following to add another service recovery workflow:
#### service_name

Do you have access to the connected recovery email/mobile?

- [Yes](#I_have_access_to_recovery_email_google)
- [No](#Recovery_Form_google)

### I_have_access_to_recovery_email_google

Check if you received a "[Password Change Email Subject]" email from service_name. Did you receive it?

- [Yes](#Email_received_service_name)
- [No](#Recovery_Form_service_name

### Email_received_service_name

> Please check if there is a "recover your account" link. Is it there?

- [Yes](#Recovery_Link_Found_service_name)
- [No](#Recovery_Form_service_name)

### Recovery_Link_Found_service_name

> Please use the [Recovery Link Description](URL) link to recover your account.

Were you able to recover your account with "[Recovery Link Description]" link?

- [Yes](#resolved_end)
- [No](#Recovery_Form_service_name)

### Recovery_Form_service_name

> Please try this recovery form to recover this account: [Link to the standard recovery form].
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

-->
