---
name: Greenhost
website: https://greenhost.net
logo:
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: 24/7, UTC+2
response_time: 4 horas
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

Greenhost ofrece servicios de tecnología de la Información (IT por sus siglas en Inglés) con un enfoque ético y sostenible. Nuestras ofertas de servicios incluyen alojamiento web, servicios en la nube y ofertas relevantes relacionadas con seguridad de la información. Al colaborar con organizaciones culturales y pioneros técnicos, nos esforzamos por brindar a nuestros usuarios todas las oportunidades del Internet a la vez que protegemos su privacidad. Estamos activamente involucrados en el desarrollo de código abierto, y participamos en varios proyectos en las áreas de tecnología, periodismo, cultura, educación, sostenibilidad y libertad del Internet.
