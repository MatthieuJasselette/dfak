---
layout: page
title: "No puedo acceder a mi cuenta"
author: RaReNet
language: es
summary: "¿Tienes problemas para acceder a un correo electrónico, a una red social o a una cuenta web? ¿Alguna cuenta muestra actividad que no reconoces? Hay muchas cosas que puedes hacer para enfrentar este problema."
date: 2019-08
permalink: /es/topics/account-access-issues/
parent: /es/
---


# No puedo acceder a mi cuenta

Las redes sociales y canales de comunicación son ampliamente utilizados por miembros de la sociedad civil  para comunicarse, compartir conocimientos y defender sus causas. Como consecuencia, dichas cuentas son fuertemente monitoreadas por actores maliciosos, que a menudo intentan comprometerlas, causando daños a los miembros de la sociedad civil y sus contactos.

Esta guía existe para ayudarte en caso de que hayas perdido el acceso a una de tus cuentas por haber sido comprometida.

Aquí dispones de un cuestionario para identificar la naturaleza de tu problema y encontrar posibles soluciones.

## Workflow

### Password_Typo

> En ocasiones, es posible que no podamos iniciar sesión en nuestra cuenta porque estamos escribiendo mal la contraseña, porque nuestra configuración de idioma del teclado no es la que usamos habitualmente o porque tenemos activado el bloqueo de mayúsculas o CapsLock.
>
> Intenta escribir tu nombre de usuario y contraseña en un editor de texto y copialos del editor para pegarlos en el formulario de inicio de sesión. También asegúrate de que la configuración de idioma de tu teclado sea la correcta, o incluso si tienes activado el bloqueo de mayúsculas.

¿Las sugerencias anteriores te ayudaron a iniciar sesión en tu cuenta?

- [Sí](#resolved_end)
- [No](#What_Type_of_Account_or_Service)

### What_Type_of_Account_or_Service

¿A qué tipo de cuenta o servicio has perdido el acceso?

- [Facebook](#Facebook)
- [Página de Facebook](#Facebook_Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#Protonmail)
- [Instagram](#Instagram)
<!--- - [AddOtherServiceLink](#service_Name) -->


### Facebook_Page

¿La página tiene otros administradores?

- [Sí](#Other_admins_exist)
- [No](#Facebook_Page_recovery_form)

### Other_admins_exist

¿Los otros administradores tienen el mismo problema?

- [Sí](#Facebook_Page_recovery_form)
- [No](#Other_admin_can_help)

### Other_admin_can_help

> Por favor, pídele a los otros administradores que te agreguen nuevamente como administrador de la página.

¿Esto solucionó el problema?

- [Sí](#Fb_Page_end)
- [No](#account_end)


### Facebook_Page_recovery_form

> Por favor, Inicia sesión en Facebook y utiliza [este formulario para recuperar la página](https://www.facebook.com/help/contact/164405897002583)).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.


¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

¿Tienes acceso al correo electrónico / móvil de recuperación asociado?

- [Sí](#I_have_access_to_recovery_email_google)
- [No](#Recovery_Form_google)

### I_have_access_to_recovery_email_google

Comprueba si recibiste un correo electrónico de "Alerta de seguridad crítica para tu cuenta de Google vinculada" de Google. ¿Lo recibiste?

- [Sí](#Email_received_google)
- [No](#Recovery_Form_google)

### Email_received_google

Por favor, comprueba si hay un enlace "recuperar tu cuenta". ¿Está ahí?

- [Sí](#Recovery_Link_Found_google)
- [No](#Recovery_Form_google)

### Recovery_Link_Found_google

> Por favor, utiliza el enlace "recuperar tu cuenta" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery_Form_google)

### Recovery_Form_google

> Por favor, prueba [este formulario de recuperación para recuperar tu cuenta](https://support.google.com/accounts/answer/7682439?hl=es).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

¿Tienes acceso al correo electrónico / móvil de recuperación conectado?

- [Sí](#I_have_access_to_recovery_email_yahoo)
- [No](#Recovery_Form_Yahoo)

### I_have_access_to_recovery_email_yahoo

Comprueba si recibiste un correo electrónico de "Cambio de contraseña para tu cuenta de Yahoo" de Yahoo. ¿Lo recibiste?

- [Sí](#Email_received_yahoo)
- [No](#Recovery_Form_Yahoo)

### Email_received_yahoo

Por favor, Verifica si hay un enlace "Recupera tu cuenta aquí". ¿Está ahí?

- [Sí](#Recovery_Link_Found_Yahoo)
- [No](#Recovery_Form_Yahoo)

### Recovery_Link_Found_Yahoo

> Por favor, utiliza el enlace "recuperar tu cuenta" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery_Form_Yahoo)

### Recovery_Form_Yahoo

> Por favor, Sigue [estas instrucciones para recuperar tu cuenta](https://es.ayuda.yahoo.com/kb/account/Solucionar-problemas-para-iniciar-sesi%C3%B3n-en-tu-cuenta-de-Yahoo-sln2051.html).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

¿Tienes acceso al correo electrónico / móvil de recuperación asociado?

- [Sí](#I_have_access_to_recovery_email_Twitter)
- [No](#Recovery_Form_Twitter)

### I_have_access_to_recovery_email_Twitter

Verifica si recibiste un correo electrónico de "Tu contraseña de Twitter ha sido cambiada" de Twitter. ¿Lo recibiste?

- [Sí](#Email_received_Twitter)
- [No](#Recovery_Form_Twitter)

### Email_received_Twitter

Por favor, Comprueba si el mensaje contiene un enlace "recuperar tu cuenta". ¿Está ahí?

- [Sí](#Recovery_Link_Found_Twitter)
- [No](#Recovery_Form_Twitter)

### Recovery_Link_Found_Twitter

> Utiliza el enlace "recuperar tu cuenta" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery_Form_Twitter)

### Recovery_Form_Twitter

> Por favor, prueba [este formulario de recuperación para recuperar esta cuenta](https://help.twitter.com/forms/restore).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


<!---=========================================================
//ProtonmailProtonmailProtonmailProtonmailProtonmailProtonmailProtonmail
//========================================================= -->

### Protonmail

> Por favor, prueba [este formulario de recuperación para recuperar tu cuenta (En Inglés)](https://protonmail.com/support/knowledge-base/reset-password/)
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

¿Tienes acceso al correo electrónico / móvil de recuperación asociado?

- [Sí](#I_have_access_to_recovery_email_Hotmail)
- [No](#Recovery_Form_Hotmail)

### I_have_access_to_recovery_email_Hotmail

Comprueba si recibiste un correo electrónico de "cambio de contraseña de cuenta de Microsoft" de Hotmail. ¿Lo recibiste?

- [Sí](#Email_received_Hotmail)
- [No](#Recovery_Form_Hotmail)

### Email_received_Hotmail

Comprueba si el mensaje contiene un enlace "Restablecer tu contraseña". ¿Está ahí?

- [Sí](#Recovery_Link_Found_Hotmail)
- [No](#Recovery_Form_Hotmail)

### Recovery_Link_Found_Hotmail

> Utiliza el enlace "Restablecer tu contraseña" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta con el enlace "Restablecer tu contraseña"?

- [Sí](#resolved_end)
- [No](#Recovery_Form_Hotmail)

### Recovery_Form_Hotmail

> Por favor, prueba [este formulario de recuperación para recuperar tu cuenta](https://account.live.com/acsr).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


### Facebook

¿Tienes acceso al correo electrónico / móvil de recuperación asociado?

- [Sí](#I_have_access_to_recovery_email_Facebook)
- [No](#Recovery_Form_Facebook)

### I_have_access_to_recovery_email_Facebook

Comprueba si recibiste un correo electrónico de "cambio de contraseña de Facebook" de Facebook. ¿Lo recibiste?

- [Sí](#Email_received_Facebook)
- [No](#Recovery_Form_Facebook)

### Email_received_Facebook

¿El correo electrónico contiene un mensaje que dice "Si no hiciste esto, asegura tu cuenta" con un enlace?

- [Sí](#Recovery_Link_Found_Facebook)
- [No](#Recovery_Form_Facebook)

### Recovery_Link_Found_Facebook

> Utiliza el enlace "Recuperar tu cuenta aquí" en el mensaje para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta haciendo clic en el enlace?

- [Sí](#resolved_end)
- [No](#Recovery_Form_Facebook)

### Recovery_Form_Facebook

> Por favor, prueba [este formulario de recuperación para recuperar tu cuenta](https://es-la.facebook.com/login/identify).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

¿Tienes acceso al correo electrónico / móvil de recuperación asociado?

- [Sí](#I_have_access_to_recovery_email_Instagram)
- [No](#Recovery_Form_Instagram)

### I_have_access_to_recovery_email_Instagram

Comprueba si recibiste un correo electrónico de "Tu contraseña de Instagram ha sido cambiada" de Instagram. ¿Lo recibiste?

- [Sí](#Email_received_Instagram)
- [No](#Recovery_Form_Instagram)

### Email_received_Instagram

Por favor, comprueba si hay un enlace de recuperación. ¿Está ahí?

- [Sí](#Recovery_Link_Found_Instagram)
- [No](#Recovery_Form_Instagram)

### Recovery_Link_Found_Instagram

> Por favor, utilice el enlace "Recuperar tu cuenta aquí" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery_Form_Instagram)

### Recovery_Form_Instagram

> Por favor, prueba [este formulario de recuperación para recuperar tu cuenta](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


### Fb_Page_end

Estamos muy contentos de que tu problema esté resuelto. Lee estas recomendaciones para ayudarte a minimizar la posibilidad de que pierdas el acceso a tu página en el futuro:

- Activar la autenticación en dos factores (o 2FA) para todos los administradores de la página.
- Asignar roles de administrador solo a las personas en las que confías y que sean rápidas ante incidentes.

### account_end

Si los procedimientos sugeridos en este flujo de trabajo no te han ayudado a recuperar el acceso a tu cuenta, puedes comunicarte con las siguientes organizaciones para solicitar más ayuda:

:[](organisations?services=account)

### resolved_end

Esperamos que esta guía del kit de primeros auxilios digitales DFAK te haya sido útil. Envíanos tus comentarios [por correo electrónico](mailto: incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Lee estas recomendaciones para ayudar a minimizar la posibilidad de perder el acceso a tus cuentas en el futuro.

- Siempre es un buen consejo activar la autenticación de dos factores (2FA) para todas las cuentas que lo permitan.
- Nunca uses la misma contraseña para más de una cuenta. Si lo haces, cámbialas lo antes posible.
- El uso de un administrador de contraseñas te ayudará a crear y recordar contraseñas únicas y seguras para todas tus cuentas.
- Ten cuidado al usar redes wifi públicas abiertas que no sean de confianza y, en cualquier caso conéctate a ellas a través de un res privada virtual (VPN) o Tor.

#### resources

* [Security in a box - Crear y mantener contraseñas fuertes](https://securityinabox.org/es/guide/passwords/)
* [Surveillance Self-Defense: Protegiéndose en las Redes Sociales](https://ssd.eff.org/es/module/protegi%C3%A9ndose-en-las-redes-sociales)