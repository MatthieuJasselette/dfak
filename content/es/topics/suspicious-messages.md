---
layout: page
title: "Recibí un mensaje sospechoso"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: es
summary: "Recibí un mensaje sospechoso, un enlace o correo electrónico, ¿qué debo hacer al respecto?"
date: 2019-08
permalink: /es/topics/suspicious-messages
parent: /es/
---

# Recibí un mensaje sospechoso

Puedes recibir mensajes _sospechosos_ en tu bandeja de entrada de correo electrónico, cuentas de redes sociales y/o aplicaciones de mensajería. La forma más común de correos electrónicos sospechosos se conocen como phishing. Este tipo de correos suplantan la identidad de otros y pretenden engañarte para que des tu información personal, financiera o de cuentas. Pueden pedirte que visites un sitio web o llames a un número de servicio al cliente falso. Los correos electrónicos de phishing también pueden contener archivos adjuntos que instalan software malicioso en tu computadora cuando se abren.

Si no estás seguro de la autenticidad del mensaje que recibes, o qué hacer al respecto, puedes usar el siguiente cuestionario como herramienta para diagnosticar la situación o compartir el mensaje con organizaciones externas de confianza que te proporcionarán un análisis más detallado del mensaje.

Ten en cuenta que recibir un correo electrónico sospechoso no significa necesariamente que tu cuenta haya sido comprometida. Si crees que un correo electrónico o un mensaje es sospechoso, no lo abras. No respondas al correo electrónico, no hagas clic en ningún enlace y no descargues ningún archivo adjunto.

## Workflow

### intro

¿Has realizado alguna acción en el mensaje o enlace?

- [Hice clic en un enlace](#link_clicked)
- [Ingresé mis credenciales](#account_security_end)
- [Descargué un archivo](#device_security_end)
- [Respondí con información](#reply_personal_info)
- [No he tomado ninguna acción todavía](#do_you_know_sender)

### do_you_know_sender

¿Reconoces el remitente del mensaje? Ten en cuenta que el remitente del mensaje pudiera ser [falsificado](https://es.wikipedia.org/wiki/Email_spoofing) para parecer alguién en quien confías.

 - [Es una persona u organización que conozco.](#known_sender)
 - [Es un proveedor de servicios (como un proveedor de correo electrónico, hosting, redes sociales o bancos).](#service_provider)
 - [El mensaje fue enviado por una persona u organización desconocida](#share)

### known_sender

> ¿Puedes llegar al remitente utilizando otro canal de comunicación? Por ejemplo, si recibiste un correo electrónico, ¿puedes verificarlo directamente con el remitente por teléfono o WhatsApp? Asegúrate de utilizar un método de contacto existente. No necesariamente puedes confiar en un número de teléfono en la firma de un mensaje sospechoso.

¿Confirmaste que el remitente es quien te envió este mensaje?

 - [Sí](#resolved_end)
 - [No](#share)

### service_provider

> En este escenario, un proveedor de servicios es cualquier compañía o marca que brinde servicios que usas o estás suscrito o suscrita. Esta lista puede incluir a tu proveedor de correo electrónico (Google, Yahoo, Microsoft, Prontonmail...), tu proveedor de redes sociales (Facebook, Twitter, Instagram...), o plataformas en línea que tienen tu información financiera (Paypal, Amazon, bancos, Netflix...).
>
> ¿Hay alguna forma de verificar si el mensaje es auténtico? Muchos proveedores de servicios también proporcionarán copias de notificaciones u otros documentos en la página de su cuenta. Por ejemplo, si el mensaje es de Facebook, debe incluirse en su [lista de correos electrónicos de notificación](https://www.facebook.com/settings?tab=security&section=recent_emails), o si es de tu banco, Puedes llamar a servicio al cliente.

Elije una de las siguientes opciones:

 - [Pude verificar que es un mensaje legítimo de mi proveedor de servicios](#resolved_end)
 - [No pude verificar el mensaje](#share)
 - [No estoy suscrito a este servicio y/o estoy esperando un mensaje de ellos](#share)

### link_clicked

> En algunos mensajes sospechosos, los enlaces pueden llevarte a páginas de inicio de sesión falsas que te robarán credenciales, u otro tipo de páginas que podrían robar tu información personal o financiera. El enlace puede, en algunos casos, pedirte que descargues archivos adjuntos que instalarán software malicioso en tu computadora cuando se abran.

¿Puedes decir lo que pasó después de hacer clic en el enlace?

 - [Me pidió ingresar credenciales](#account_security_end)
 - [Descargó un archivo](#device_security_end)
 - [No pasó nada pero no estoy seguro](#device_security_end)
 - [No hice clic en el enlace](#share)

### reply_personal_info

> Dependiendo del tipo de información que compartiste, es posible que debas tomar medidas inmediatas.

¿Qué tipo de información compartiste?

- [Compartí información confidencial de una cuenta](#account_security_end)
- [Compartí información pública](#share)
- [No estoy seguro de cuán sensible era la información y necesito ayuda](#help_end)


### share

> Compartir tu mensaje sospechoso puede ayudar a proteger a tus colegas y a comunidades que también pueden verse afectadas.
>
> Para compartir tu mensaje sospechoso, asegúrate de incluir el mensaje en sí, así como la información sobre el remitente. Si el mensaje era un correo electrónico, asegúrate de incluir el correo electrónico completo, incluidos los encabezados, utilizando la [siguiente guía de CIRCL (En Inglés)](https://www.circl.lu/pub/tr-07/).

¿Necesitas más ayuda?

- [Sí, necesito mas ayuda](#help_end)
- [No, he resuelto mi problema](#resolved_end)


### device_security_end

> ¡En caso de que algunos archivos se hayan descargado en tu dispositivo, la seguridad de este puede correr peligro!

Por favor, contacta las siguientes organizaciones que pueden ayudarte. Luego, [comparte el mensaje sospechoso](#share).

:[](organisations?services=device_security)


### account_security_end

> En caso de que hayas ingresado tus credenciales, ¡la seguridad de tus cuentas puede estar en riesgo!
>
> Si crees que tu cuenta está comprometida, te recomendamos que también sigas el cuestionario de la guía de primeros auxilios digitales DFAK sobre [cuentas comprometidas](../../../account-access-issues).
>
> Te sugerimos que informes a tu comunidad sobre esta campaña de phishing y compartas el mensaje sospechoso con organizaciones que puedan analizarlo.

¿Deseas compartir información sobre el mensaje que recibiste o necesitas ayuda adicional?

- [Quiero compartir el mensaje sospechoso](#share)
- [Necesito más ayuda para asegurar mi cuenta](#account_end)
- [Necesito mas ayuda para analizar el mensaje](#analysis_end)

### help_end

> Debes buscar ayuda de tus colegas u otras personas para comprender mejor los riesgos de compartir dicha información. Otras personas en tu organización o red también pueden haber recibido mensajes similares.

Por favor, contacta a las siguientes organizaciones que pueden ayudarte. Luego, [comparte el mensaje sospechoso](#share).

:[](organisations?services=digital_support)


### account_end

Si tu cuenta ha sido comprometida y necesitas ayuda para protegerla, comunicate con las organizaciones a continuación descritas que pueden ayudarte.

:[](organisations?services=account)


### analysis_end

Las siguientes organizaciones pueden recibir tu mensaje sospechoso e investigar a fondo en tu nombre:

:[](organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

Esperamos que esta guía de primeros auxilios digitales (DFAK) te haya sido útil. Por favor, danos tu opinión [vía email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

La primera regla a recordar: nunca divulgues información personal en correos electrónicos. Ninguna institución, banco u otras organizaciones solicitarán esta información por correo electrónico. Puede que no siempre sea fácil saber si un correo electrónico o sitio web es legítimo, pero hay algunos consejos que pueden ayudarte a evaluar lo que recibiste.

* Sentido de urgencia: los correos electrónicos sospechosos generalmente advierten de un cambio repentino en una cuenta y te piden que actúes de inmediato para verificar tu cuenta.
* En el cuerpo de un correo electrónico, puedes ver preguntas que te piden que "verifiques" o "actualices tu cuenta" o que "la falta de actualización de tus registros conlleva la suspensión de la cuenta". Por lo general, es seguro asumir que ninguna organización creíble a la que hayas proporcionado tu información te pedirá que la vuelvas a ingresar, no caigas en la trampa.
* Cuidado con los mensajes no solicitados, archivos adjuntos, enlaces y páginas de inicio de sesión.
* Cuidado con los errores de ortografía y gramática.
* Haga clic para ver la dirección completa del remitente, no solo el nombre que se muestra.
* Ten en cuenta los enlaces acortados, ya que pueden ocultar enlaces maliciosos.
* Cuando pasas el mouse sobre un enlace, la URL real a la que te dirigen se muestra en una ventana emergente o en la parte inferior de la ventana de tu navegador.

#### Resources

Aquí van una serie de recursos para detectar mensajes sospechosos y evitar ser engañados.

* [EFF: Cómo evitar los ataques de phishing o suplantación de identidad](https://ssd.eff.org/es/module/c%C3%B3mo-evitar-los-ataques-de-phishing-o-suplantaci%C3%B3n-de-identidad)
* [PhishMe: Cómo detectar una infografía Phishing (En Inglés)](https://cofense.com/wp-content/uploads/2016/07/phishme-how-to-spot-a-phish.pdf)
* [Citizen Lab: Comunidades en riesgo (En Inglés)](https://targetedthreats.net) - [Appendix](https://targetedthreats.net/media/5-Appendix.pdf) Incluye ejemplos de correos electrónicos de phishing reales con una clasificación de qué tan personalizados y específicos son.
