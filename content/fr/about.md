---
layout: page.pug
title: "A propos"
language: fr
summary: "A propos du kit de premiers soins numériques."
date: 2019-03-13
permalink: /fr/about/
parent: Home
---

La trousse de premiers soins numériques est le fruit d'une collaboration entre le [RaReNet (Rapid Response Network)](https://www.rarenet.org/) et le [CiviCERT](https://www.civicert.org/).

Le Réseau de réponse rapide est un réseau international d'intervenants réactifs et d'experts en sécurité numérique qui comprend l'EFF, Global Voices, Hivos & the Digital Defenders Partnership, Front Line Defenders, Internews, Freedom House, Access Now, Virtual Road, CIRCL, Open Technology Fund, ainsi que des experts individuels en sécurité qui travaillent dans le domaine de la sécurité numérique et des interventions d'urgence.

Certaines de ces organisations et personnes font partie de CiviCERT, un réseau international de services d'assistance en matière de sécurité numérique et de fournisseurs d'infrastructures qui se concentrent principalement sur le soutien aux groupes et organisations luttant pour la justice sociale et la défense des droits humains et numériques. CiviCERT est un cadre professionnel pour soutenir les efforts distribués de la communauté d'intervention rapide CERT (Computer Emergency Response Team). CiviCERT est accrédité par Trusted Introducer, le réseau européen d'équipes de confiance pour l'intervention d'urgence en informatique.

La trousse de premiers soins numérique est également un projet [open-source qui accepte des contributions extérieures](https://gitlab.com/rarenet/dfak).

