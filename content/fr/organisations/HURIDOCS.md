---
name: HURIDOCS
website: https://www.huridocs.org
logo: huridocs-signature-logo.jpg
languages: Español, English, Русский, Français, العربية, Deutsch, Nederlands, հայերէն, Polski
services: in_person_training, org_security, assessment, secure_comms, account, device_security
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7
response_time: 12-48 heures
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.huridocs.org/contacts_2/
email: info@huridocs.org
pgp_key: E7E503AB
phone: 0041227555252
mail: Rue de Varembe 3, 1202 Geneva, Switzerland
initial_intake: 
---

HURIDOCS aide les défenseurs des droits humains à utiliser l'information et la technologie pour
faire la lumière sur les abus et promouvoir la justice à la fois pour les victimes et pour les auteurs de violations.
