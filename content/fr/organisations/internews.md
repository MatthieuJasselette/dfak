---
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, global
response_time: 12 hours
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

En complément du travail de base d'Internews, Internews travaille également avec des individus, des organisations et des communautés du monde entier pour accroître la sensibilisation à la sécurité numérique, protéger l'accès à un Internet ouvert et non censuré, et améliorer les pratiques de sécurité numérique. 

Internews a ainsi formé des milliers de journalistes et de défenseurs des droits de l'homme dans plus de 80 pays, et dispose de solides réseaux de formateurs locaux et régionaux en sécurité numérique et d'auditeurs familiers avec le cadre [SAFETAG Security Auditing Framework and Evaluation Template for Advocacy Groups](https://safetag.org), dont Internews a conduit le développement. 

Internews établit des partenariats solides et réactifs avec la société civile et des entreprises d'analyse et de renseignement des menaces, et peut aider directement ses partenaires à maintenir une présence en ligne, sûre et non censurée. 

Internews propose des interventions techniques et non techniques allant de l'évaluation de base de la sécurité à l'aide du cadre SAFETAG, en passant par l'évaluation des politiques organisationnelles et la révision des stratégies de réduction des risques fondées sur la recherche de menaces. Nous soutenons directement l'analyse de logiciels malveillants et des pratiques d'hameçonnage pour les groupes de défense des droits humains et les médias qui subissent des attaques numériques ciblées.
