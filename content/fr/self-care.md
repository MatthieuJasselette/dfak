---
layout: sidebar.pug
title: "Se sentir surmené ?"
author: FP
language: fr
summary: "Le harcèlement en ligne, les menaces et autres types d'attaques numériques peuvent créer des sentiments d'accablement, de surmenage et des états émotionnels très délicats : vous pouvez vous sentir coupable, honteux, anxieux, en colère, confus, impuissant ou même avoir peur pour votre bien-être psychologique ou physique."
date: 2018-09-05
permalink: /fr/self-care/
parent: Home
sidebar: >
  <h3>Pour en savoir plus sur la façon de vous protéger contre le sentiment de surmenage :</h3>

  <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Prendre soin de soi pour les activistes : Maintenir votre ressource la plus précieuse</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Prendre soin de vous pour que vous puissiez continuer à défendre les droits de l'homme</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Prendre soin de soi pour les personnes victimes de harcèlement</a></li>
    <li><a href="https://www.fightcyberstalking.org/emotional-support">Soutien émotionnel aux victimes de cyberharcèlement</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Module de formation à l'autogestion de la santé pour les femmes en ligne</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care/">Bien-être et communauté</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Vingt façons d'aider quelqu'un qui est victime d'intimidation en ligne</a></li>
    <li><a href="https://www.hrresilience.org/">Projet de résilience pour les défenseurs des droits humains</a></li>
  </ul>
---

# Vous vous sentez surmené(e) ?

Le harcèlement en ligne, les menaces et autres types d'attaques numériques peuvent créer des sentiments d'accablement, de surmenage et des états émotionnels très délicats : vous pouvez vous sentir coupable, honteux, anxieux, en colère, confus, impuissant ou même avoir peur pour votre bien-être psychologique ou physique.

Il n'y a pas de "bonne" façon de se sentir, car votre état de vulnérabilité et ce que vos renseignements personnels signifient pour vous est différent d'une personne à l'autre. Toute émotion est justifiée, et vous ne devriez pas vous inquiéter de savoir si votre réaction est la bonne ou non.

La première chose dont vous devez vous rappeler est que ce qui vous arrive n'est pas de votre faute et que vous ne devez pas vous blâmer, mais peut-être vous adresser à une personne de confiance qui peut vous aider à faire face à cette urgence.

Si vous avez une personne en qui vous avez confiance, vous pouvez lui demander de vous aider tout en suivant les instructions de ce site Web, ou lui donner accès à vos appareils ou comptes pour recueillir des informations à votre place.
