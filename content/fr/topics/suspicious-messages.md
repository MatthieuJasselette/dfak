---
layout: page
title: "J'ai reçu des messages suspects"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: fr
summary: "J'ai reçu un message, un lien ou un e-mail suspect, que dois-je faire ?"
date: 2019-03-12
permalink: /fr/topics/suspicious-messages
parent: /fr/
---

# I Received a Suspicious Message

Vous pouvez recevoir des messages _suspicieux_ dans votre boîte de réception d'emails, vos comptes de médias sociaux et/ou vos applications de messagerie. La forme la plus courante de courriels suspects est l'hameçonnage. Les courriels d'hameçonnage visent à vous piéger et à vous amener à donner des renseignements personnels, financiers ou sur vos comptes en ligne. Ils peuvent vous demander de visiter un faux site Web ou d'appeler un faux numéro de service clientèle. Les courriels d'hameçonnage peuvent également contenir des pièces jointes qui installent des logiciels malveillants sur votre ordinateur lorsqu'elles sont ouvertes.

Si vous n'êtes pas certain de l'authenticité du message que vous avez reçu ou de ce qu'il faut faire à son sujet, vous pouvez utiliser le questionnaire suivant comme outil pour diagnostiquer la situation ou pour partager le message avec des organisations externes de confiance qui vous fourniront une analyse plus détaillée de celui-ci.

N'oubliez pas que le fait de recevoir un courriel suspect ne signifie pas nécessairement que votre compte a été compromis. Si vous pensez qu'un courriel ou un message est suspect, ne l'ouvrez pas. Ne répondez pas au courriel, ne cliquez sur aucun lien et ne téléchargez aucune pièce jointe.

## Workflow

### intro

Avez-vous effectué une action sur le message ou un lien ?

- [J'ai cliqué sur un lien](#link_clicked)
- [J'ai entré des informations d'identification](#account_security_end)
- [J'ai téléchargé un fichier](#device_security_end)
- [J'ai répondu avec des informations](#reply_personal_info)
- [Je n'ai encore rien fait](#do_you_know_sender)

### do_you_know_sender

Reconnaissez-vous l'expéditeur du message ? Sachez que l'identité de l'expéditeur du message peut être [usurpée](https://en.wikipedia.org/wiki/Email_spoofing_spoofing) pour donner l'impression d'être quelqu'un en qui vous avez confiance.

 - [C'est une personne ou une organisation que je connais](#known_sender)
 - [Il s'agit d'un fournisseur de services (tel qu'un fournisseur de courriel, un hébergeur, un fournisseur de médias sociaux ou une banque)](#service_provider)
 - [Le message a été envoyé par une personne ou une organisation inconnue](#share)

### known_sender

> Pouvez-vous joindre l'expéditeur par un autre canal de communication ? Par exemple, si vous avez reçu un courriel, pouvez-vous vérifier directement auprès de l'expéditeur par téléphone ou par WhatsApp qu'il vous a effectivement envoyé un message ? Assurez-vous d'utiliser une méthode de contact existante. Vous ne pouvez pas nécessairement faire confiance à un numéro de téléphone dans la signature d'un message suspect.

Avez-vous confirmé que c'est l'expéditeur qui vous a envoyé ce message ?

 - [Oui](#resolved_end)
 - [Non](#share)

### service_provider

> Dans ce scénario, un fournisseur de services est toute entreprise ou marque qui fournit des services que vous utilisez ou auxquels vous êtes abonné. Cette liste peut contenir votre fournisseur de messagerie (Google, Yahoo, Microsoft, Prontonmail...), votre fournisseur de médias sociaux (Facebook, Twitter, Instagram...), ou des plateformes en ligne contenant vos informations financières (Paypal, Amazon, banques, Netflix...).
>
> Existe-t-il un moyen de vérifier si le message est authentique ? De nombreux fournisseurs de services vous fourniront également des copies d'avis ou d'autres documents dans la page de votre compte. Par exemple, si le message provient de Facebook, il doit être inclus dans votre [liste des emails de notification](https://www.facebook.com/settings?tab=security&section=recent_emails=security&section=recent_emails), ou s'il provient de votre banque, vous pouvez appeler votre service clientèle.

Choisissez l'une des options ci-dessous :

 - [J'ai pu vérifier qu'il s'agit d'un message légitime de mon fournisseur de services](#resolved_end)
 - [Je n'ai pas pu vérifier le message](#share)
 - [Je ne suis pas abonné à ce service ni en attente d'un message de leur part](#share)

### link_clicked

> Dans certains messages suspects, les liens peuvent vous conduire à de fausses pages de connexion qui vous voleront vos identifiants ou d'autres types de pages qui pourraient voler vos informations personnelles ou financières. Dans certains cas, le lien peut vous demander de télécharger des pièces jointes qui installent des logiciels malveillants sur votre ordinateur lorsqu'elles sont ouvertes.

Pouvez-vous dire ce qui s'est passé après que vous ayez cliqué sur le lien ?

 - [Il m'a demandé d'entrer des informations d'identification](#account_security_end)
 - [Un fichier a été téléchargé](#device_security_end)
 - [Rien ne s'est passé mais je ne suis pas sûr(e)](#device_security_end)
 - [Je n'ai pas cliqué sur le lien](#share)

### reply_personal_info

> Selon le type d'informations que vous avez partagées, il se peut que vous deviez prendre des mesures immédiates.

Quel genre d'informations avez-vous partagées ?

- [J'ai partagé des informations de compte confidentielles](#account_security_end)
- [J'ai partagé des informations publiques](#share)
- [Je ne suis pas sûr(e) à quel point les informations étaient sensibles et j'ai besoin d'aide](#help_end)


### share

> Partager votre message suspect peut aider à protéger vos collègues et la communauté qui pourrait également être affectée.
>
> Pour partager votre message suspect, assurez-vous d'inclure le message lui-même ainsi que des informations sur l'expéditeur. S'il s'agit d'un courriel, veuillez vous assurer d'inclure le courriel complet, y compris les en-têtes, en utilisant le [guide suivant de la CIRCL](https://www.circl.lu/pub/tr-07/).

Avez-vous besoin d'aide supplémentaire ?

- [Oui, j'ai besoin de plus d'aide](#help_end)
- [Non, j'ai résolu mon problème](#resolved_end)


### device_security_end

> Si certains fichiers ont été téléchargés sur votre appareil, la sécurité de votre appareil peut être compromise !

Veuillez communiquer avec les organismes ci-dessous qui peuvent vous aider. Par la suite, veuillez [partager votre message suspect](#share).

:[](organisations?services=device_security)


### account_security_end

> Si vous avez entré vos identifiants, la sécurité de vos comptes peut être compromise !
>
> Si vous pensez que votre compte est compromis, nous vous recommandons de suivre également le flux de travail de la trousse de premiers soins numérique sur les [comptes compromis](../../../account-access-issues).
>
> Nous vous suggérons d'informer votre communauté au sujet de cette campagne d'hameçonnage et de partager le message suspect avec les organismes qui peuvent les analyser.

Souhaitez-vous partager des informations sur le message que vous avez reçu ou avez-vous besoin d'aide supplémentaire ?

- [Je veux partager le message suspect](#share)
- [J'ai besoin de plus d'aide pour sécuriser mon compte](#account_end)
- [J'ai besoin de plus d'aide pour analyser le message](#analysis_end)


### help_end

> Vous devriez demander l'aide de vos collègues ou d'autres personnes pour mieux comprendre les risques associés à l'information que vous avez partagée. D'autres personnes de votre organisation ou de votre réseau peuvent également avoir reçu des demandes similaires.

Veuillez communiquer avec les organismes ci-dessous qui peuvent vous aider. Par la suite, veuillez [partager votre message suspect](#share).

:[](organisations?services=24_7_digital_support)


### account_end

Si votre compte a été compromis et que vous avez besoin d'aide pour le sécuriser, veuillez contacter les organisations ci-dessous qui peuvent vous aider.

:[](organisations?services=account)


### analysis_end

Les organisations suivantes peuvent recevoir votre message suspect et enquêter pour vous :

:[](organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

Nous espérons que ce guide des premiers soins numériques (DFAK) a été utile. Veuillez nous faire part de vos commentaires [par email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)


### final_tips

La première règle à retenir : Ne donnez jamais de renseignements personnels par email. Aucune institution, banque ou autre ne vous demandera jamais ces renseignements par mail. Il n'est pas toujours facile de savoir si un email ou un site Web est légitime, mais certains conseils peuvent vous aider à évaluer le courriel que vous avez reçu.

* Sentiment d'urgence : les emails suspects vous avertissent généralement d'un changement soudain sur un compte et vous demandent d'agir immédiatement pour vérifier ce compte.
* Dans le corps d'un courriel, vous pouvez voir des questions vous demandant de "vérifier" ou de "mettre à jour votre compte" ou "ne pas mettre à jour vos informations entraînera la suspension de votre compte". Il est généralement sûr de supposer qu'aucune organisation crédible à laquelle vous avez fourni vos renseignements ne vous demandera jamais de les entrer à nouveau, alors ne tombez pas dans ce piège.
* Méfiez-vous des messages non sollicités, des pièces jointes, des liens et des pages de connexion.
* Attention aux fautes d'orthographe et de grammaire.
* Cliquez pour afficher l'adresse complète de l'expéditeur, pas seulement le nom affiché.
* Attention aux liens raccourcis - ils peuvent cacher un lien malveillant derrière eux.
* Lorsque vous passez votre souris sur un lien, l'URL vers laquelle vous êtes dirigé s'affiche dans un popup ou au bas de la fenêtre de votre navigateur.


#### Resources

Voici un certain nombre de ressources pour repérer les messages suspects et éviter l'hameçonnage.

* [EFF : Éviter les attaques par hameçonnage](https://ssd.eff.org/fr/module/guide-pratique-%C3%A9viter-les-attaques-par-hame%C3%A7onnage)
* [PhishMe: Une infographie sur Comment identifier un hammeçonnage](https://cofense.com/wp-content/uploads/2016/07/phishme-how-to-spot-a-phish.pdf)
* [Citizen Lab: Communities @ Risk report](https://targetedthreats.net) - [Annexe](https://targetedthreats.net/media/5-Appendix.pdf) comprend des exemples de courriels d'hameçonnage réels, y compris une catégorisation de leur degré de personnalisation et de ciblage.
