---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: ddhs, oscs
hours: emergência 24/7, global; Horário comercial Segunda-Sexta, IST (UTC+1), equipe localizada em diferentes fuso-horários e regiões
response_time: no mesmo dia ou no próximo em casos de emergência
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 for emergencies; +353-1-212-3750 office phone
skype: front-line-emergency?call
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

A Front Line Defenders (FLD) é uma organização internacional baseada na Irlanda que atua com proteção integrada a defensores de direitos humanos em situação de risco. A FLD fornece suporte rápido a defensores em risco imediato através de financiamentos de segurança, treinamento de segurança física e digital, advocacy e apoio de campanha.

A Front Line Defenders opera uma linha de suporte a emergência 24/7 através do +353-121-00489 para situações de risco imediato em idioma Arábico, Inglês, Francês, Russo ou Espanhol. Quando defensores de direitos humanos encaram ameaça imediata às suas vidas, a FLD pode auxiliar com realocação temporária. Além disso, a FLD publiciza os casos destes defensores, criando campanhas e pressionando governos, órgãos interregionais e advogando por sua proteção.
