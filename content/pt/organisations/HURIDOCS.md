---
name: HURIDOCS
website: https://www.huridocs.org
logo: huridocs-signature-logo.jpg
languages: Español, English, Русский, Français, العربية, Deutsch, Nederlands, հայերէն, Polski
services: in_person_training, org_security, assessment, secure_comms, account, device_security
beneficiaries: jornalistas, ddhs, ativistas, lgbti, mulheres, jovens, cso
hours: 24/7
response_time: 12-48 horas
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.huridocs.org/contacts_2/
email: info@huridocs.org
pgp_key: E7E503AB
phone: 0041227555252
mail: Rue de Varembe 3, 1202 Geneva, Switzerland
initial_intake:
---

HURIDOCS helps human rights defenders use information and technology to
shine a light on abuses and promote justice for both victims and
perpetrators of violations.

HURIDOCS auxilia defensores de direitos humanos a usar informação e tecnologia para elucidar abusos e promover justiça para as vítimas e com relação aos perpetradores destas violações
