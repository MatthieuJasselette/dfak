---
layout: page
title: "Outra pessoa está se passando por mim online"
author: Floriana Pagano, Alexandra Hache
language: pt
summary: "Uma pessoa está tendo sua identidade roubada através de uma conta de mídia social, endereço de email, chave PGP, site ou blog falso ou app"
date: 2019-04-01
permalink: /pt/topics/impersonated
parent: /pt/
---

# Outra pessoa está se passando por mim online

Uma ameaça enfrentada por muitos ativistas, defensores des direitos humanos, ONGs, mídias independentes e produtores de conteúdo é ter a identidade roubada por adversários que criam perfis falsos, sites ou e-mails em seus nomes. Na maioria dos casos, o objetivo disso é criar campanhas de difamação, promover notícias falsas, facilitar práticas de engenharia social ou roubo de identidade, ou mesmo somente gerar desconfiança e violar dados que impactam na reputação dos indivíduos e coletivos que estão sendo personificados. Este é um problema que pode causar diversos impactos emocionais e ter diferentes causas e graus de consequências dependendo de como afeta suas comunicações e em quais meios estão roubando suas identidades.

É importante saber que existem muitas maneiras de se passar por alguém (perfis falsos nas redes sociais, sites clonados, e-mails falsificados, publicação não-consensual de imagens pessoais e vídeos). As estratégias podem variar desde o envio de ordens de remoção do site, disputa de propriedade do domínio, a reivindicação de direitos autorais do site ou das informações postadas, ou ameaças às suas redes e contatos pessoais através de mensagens públicas ou confidenciais. Diagnosticar o problema e encontrar possíveis soluções para personificação pode ser complicado. Em certos casos será quase impossível convencer uma pequena empresa de hospedagem a derrubar um site, e mover uma ação legal pode se tornar necessário. É uma boa prática configurar alertas e monitorar a internet para descobrir se você ou sua organização estão sendo personificados.

This section of the Digital First Aid Kit will walk you through some basic steps to diagnose potential ways of impersonating and potential mitigation strategies to remove accounts, websites and emails impersonating you or your organization.

If you are being impersonated, follow this questionnaire to identify the nature of your problem and find possible solutions.

Esta seção do Kit de Primeiros Socorros Digitais irá guiar você através de alguns passos básicos para diagnosticar potenciais formas de personificação e potenciais estratégias de mitigação para remover contas, sites e emails que se passam por você ou sua organização.

Se alguém está tomando sua identidade online, siga o questionário para identificar a natureza do problema e tentar encontrar possíveis soluções.



## Workflow

### urgent_question

Você teme por sua integridade física e bem estar?

 - [Sim](#physical_sec_end)
 - [Não](#diagnostic_start1)

### diagnostic_start1

A personificação está te afetando como indivíduo (alguém está usando seu nome legal, social ou notório) ou como coletivo/organização?

- [Como indivíduo](#individual)
- [Como organização](#organization)

### individual

Se estão afetando você como indivíduo, considere alertar seus contatos. Faça isso usando uma conta de email, perfil ou site que esteja sob seu total controle.

- Uma vez que você informe seus contatos que estão se pasando por você, siga para o [próximo passo](#diagnostic_start2).

### organization

> Se estão afetando você como grupo, é importante fazer uma anúncio público. Faça isso usando uma conta de email, perfil ou site que esteja sob seu total controle.

- Uma vez que tenha informado sua comunidade que estão se pasando por você, siga para o [próximo passo](#diagnostic_start2).

### diagnostic_start2

De que forma estão se passando por você?

 - [Um site fake está usando meu nome ou o da minha organização/coletivo](#fake_website)
 - [Através de uma conta de rede social](#social_network)
 - [Compartilhando vídeos e imagens minhas sem consentimento](#other_website)
 - [Através do meu email ou endereço similar](#spoofed_email1)
 - [Através de uma chave PGP conectada ao meu email](#PGP)
 - [Através de um app falso que imita meu app](#app1)

### social_network

Em qual plataforma de mídia social estão se passando por você?

- [Facebook](#facebook)
- [Twitter](#twitter)
- [Google](#google)
- [Instagram](#instagram)

###  facebook

> Siga [estas instruções](https://pt-br.facebook.com/help/174210519303259) para solicitar que a conta falsa seja deletada.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

### twitter

> Preencha [este formulário](https://help.twitter.com/forms/impersonation) para solicitar que a conta falsa seja deletada.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

### google

> Preencha [este formulário](https://support.google.com/plus/troubleshooter/1715140?hl=pt-BR) para solicitar que a conta falsa seja deletada.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

### instagram

> Siga [estas instruções](https://help.instagram.com/446663175382270) para solicitar que a conta falsa seja deletada.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


### fake_website

> Verifique se o site já é reconhecido como malicioso buscando a URL nos serviços a seguir:
>    
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)
> - [ThreatCrowd](https://www.threatcrowd.org/)

O domínio é reconhecido como malicioso?

 - [Sim](#malicious_website)
 - [Não](#non-malicious_website)

### malicious_website

> Denuncie a URL para o Google Safe Browsing preenchendo [este formulário](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Note que pode levar algum tempo para garantir que sua denúncia teve sucesso. Neste tempo, você pode seguir para os próximos passos enviando uma ordem de remoção para os serviços de hospedagem e domínio, ou se preferir apenas salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento funcionou para você?

- [Sim](#resolved_end)
- [Não](#non-malicious_website)


### non-malicious_website

> Você pode tentar denunciar o site para os serviços de hospedagem e domínio solicitando sua retirada.
>
> Se o site que você precisa denunciar está usando o seu conteúdo, uma coisa que você deverá provar é a propriedade do conteúdo postado. Você pode comprovar istoi apresentando o contrato original com o serviço de registro de domínio ou de hospedagem, mas também prover resultados de arquivo da [Wayback Machine](https://archive.org/web/), buscando pela URL de ambos o site original em sua propriedade e o site falso. Se os sites foram indexados lá em algum momento, você terá um histórico que fará possível comprovar a existência de seu site antes da publicação do site falso.
>
> Para enviar uma solicitação de remoção do conteúdo, você precisará coletar a seguinte informação sobre o site falso:
>
> - Vá até [este site](https://network-tools.com/nslookup/) e encontre o endereço (ou endereços) IP do site falso, inserindo sua URL no formulário de busca.
> - Anote os endereços IP.
> - Siga para [este site](https://whois.domaintools.com/) e pesquise ambos o domínio e os endereços IP para o site falso.
> - Guarde o nome e endereço de email de notificação dos serviços de hospedagem e domínio. Caso esteja incluso nos resultados, guarde o nome do dono do site.
> - Escreva para ambos os serviços de hospedagem e domínio do site falso, solicitando a remoção. Em sua mensagem, inclua as informações coletadas, bem como os motivos pelos quais a existência deste site é abusiva.
> - Você pode utilizar [este modelo](https://accessnowhelpline.gitlab.io/community-documentation/352-Report_Fake_Domain_Hosting_Provider.html) para escrever ao serviço de hospedagem.
> - Você pode utilizar [este modelo](https://accessnowhelpline.gitlab.io/community-documentation/343-Report_Domain_Impersonation_Cloning.html) para escrever ao serviço de registro de domínio.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se os procedimentos funcionaram.

Estes procedimentos funcionaram para você?

- [Sim](#resolved_end)
- [Não](#web_protection_end)


### spoofed_email1

> Autenticar e-mails é uma tarefa tecnicamente complicada por uma questão de engenharia. Por conta desta dificuldade, é fácil abusar dos protocolos existentes e criar endereços forjados e mensagens iscas.

Are you being impersonated through your email address, or a similar one, for example with the same user name, but a different domain?
Alguém tomou para si o endereço de email que você usa frequentemente, ou estão usando um similar, por exemplo com o mesmo nome (escrito igual) mas com outro domínio (outro endereço/@)?

- [Tomaram o endereço de email que eu uso frequentemente](#spoofed_email2)
- [Estão usando um endereço similar para se passar por mim](#similar_email)


### spoofed_email2

> A pessoa que está se passando por você pode ter invadido sua conta de email. Para descartar esta possibilidade, troque imediatamente sua senha.

Você conseguiu trocar a sua senha?

- [Sim](#spoofed_email3)
- [Não](#hacked_account)

### hacked_account

Se você não conseguiu trocar a sua senha, sua conta de email provavelmente foi comprometida.

- Você pode seguir [este diagnóstico](../../../account-access-issues) para tratar esta questão.

### spoofed_email3

> Fraude de emails (spoofing) é uma prática que consiste em forjar um endereço de envio. A intenção é fazer parecer que a mensagem foi enviada por um remetente confiável, disfarçando sua origem falsa.
>
> Esta técnica é comum em campanhas de spam e golpes digitais (phishing), pelo fato de as pessoas estarem mais suscetíveis a abrir um e-mail quando imaginam ter vindo de uma fonte legítima.
>
> Se alguém está fraudando seu email, você deve informar seus contatos para alertá-los sobre o risco de serem atraídos para um phishing (faça isso através de outra conta de email confiável, perfil, ou site que estejam sob seu total controle).
>
> Se você acredita que a personificação da sua identidade foi focada em usos maliciosos e phishing, você pode querer ler também a seção [Recebi mensagens suspeitas](../../../suspicious_messages).

Os emails pararam após trocar a senha da sua conta?

- [Sim](#compromised_account)
- [Não](#secure_comms_end)


### compromised_account

> Provavelmente sua conta foi invadida por alguém que está enviando emails para se passar por você. Como sua conta foi comprometida, você pode querer ler também a seção [Perdi o acessso às minhas contas](../../../account-access-issues/).

Estas informações ajudaram a resolver o seu problema?

- [Sim](#resolved_end)
- [Não](#account_end)


### similar_email

> Se estão se passando por você através de um endereço de email similar ao seu mas de um domínio diferente ou com outro nome de usuário modificado, é uma boa ideia alertar seus contatos sobre a tentativa de fraude (faça isso através de outra conta de email confiável, perfil, ou site que estejam sob seu total controle).
>
> Você também pode querer ler a seção [Recebi mensagens suspeitas](../../../suspicious-messages), uma vez que esta personificação pode ser uma tentativa de fisgar contatos para um golpe (phishing).

Estas informações ajudaram a resolver o seu problema?

- [Sim](#resolved_end)
- [Não](#secure_comms_end)


### PGP

Você acredita que sua chave PGP privada possa ter sido comprometida, por exemplo por perda ou acesso indevido ao dispositivo onde estava armazenada?

- [Sim](#PGP_compromised)
- [Não](#PGP_spoofed)

### PGP_compromised

Você ainda possui acesso à sua chave privada?

- [Sim](#access_to_PGP)
- [Não](#lost_PGP)

### access_to_PGP

> - Revogue sua chave.
>     - [Instruções para o Enigmail (em inglês)](https://www.enigmail.net/index.php/en/user-manual/key-management#Revoking_your_key_pair)
> - Crie um novo par de chaves e peça a pessoas que você confia que assinem-a.
> - Através de um canal de comunicação seguro, informe seus contatos que você revogou sua chave antiga e gerou uma nova chave.

Você necessita de maior apoio para resolver este problema?

- [Sim](#secure_comms_end)
- [Não](#resolved_end)


### lost_PGP

Você tem um certificado de revogação?

- [Sim](#access_to_PGP)
- [Não](#no_revocation_cert)


### no_revocation_cert

> - Crie um novo par de chaves e peça a pessoas que você confia que assinem-a.
> - Através de um canal de comunicação seguro, informe seus contatos que você revogou sua chave antiga e gerou uma nova chave.

Você necessita de maior apoio para resolver este problema?

- [Sim](#secure_comms_end)
- [Não](#resolved_end)

### PGP_spoofed

Sua chave está assinada por pessoas de sua confiança?

- [Sim](#signed_key)
- [Não](#non-signed_key)

### signed_key

> Informe seus contatos através de um canal de comunicação seguro que alguém está tentando se passar por você e que confirmem a veracidade de sua chave conferindo as assinaturas dos contatos de confiança informados por você.

Você necessita de maior apoio para resolver este problema?

- [Sim](#secure_comms_end)
- [Não](#resolved_end)

### non-signed_key

> - Peça a pessoas que você confia que assinem sua chave.
> - Informe seus contatos através de um canal de comunicação seguro que alguém está tentando se passar por você e que confirmem a veracidade de sua chave conferindo as assinaturas dos contatos de confiança informados por você.

Você necessita de maior apoio para resolver este problema?

- [Sim](#secure_comms_end)
- [Não](#resolved_end)

### other_website

> Se alguém está usando sua identidade em um site, a primeira coisa que você precisa entender é onde este site está hospedado, quem está gerenciando, e quem está provendo o domínio. Tal pesquisa se destina a identificar a melhor forma de solicitar a remoção do conteúdo malicioso.
>
> Antes de prosseguir com a investigação, caso possua cidadania da União Européia, você pode requerer ao Google a remoção deste site de buscas relacionadas a seu nome.

Você possui cidadania da União Européia?

- [Sim](#EU_privacy_removal)
- [Não](#doxing_question)


### EU_privacy_removal

> Preencha [este formulário](https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=rtbf&hl=en&rd=1) para remover este site de buscas do Google relacionadas a seu nome.
>
> Você irá precisar de:
>
> - Uma cópia digital de um documento de identificação (se estiver enviando a solicitação em intermédio de outra pessoa, precisará enviar o documento de identificação desta pessoa)
> - As URLs dos conteúdos que deseja que sejam removidos das buscas
> - Para cada URL fornecida, você deve explicar:
>     1. como a informação pessoal contida se relaciona com a pessoa que está solicitando a remoção
>     2. porque você acredita que tal conteúdo deve ser removido
>
> Note que será necessário autenticar com sua Conta Google, e o envio do formulário será associado a ela.
>
> Após submeter o formulário, você deverá aguardar a resposta do Google para verificar se os resultados foram devidamente removidos.

Você gostaria de submeter também uma solicitação de remoção do site com informações falsas sobre você?

- [Sim](#doxing_question)
- [Não, preciso de outras formas de apoio](#account_end)

### doxing_question

A pessoa que está se passando por você publicou informações pessoais, ou fotos e videos íntimos sobre você?

- [Sim](../../../harassed-online/questions/doxing_web)
- [Não](#fake_website)

### app1

> Se alguém está espalhando uma cópia de aplicativo modificada ou um app malicioso similar ao seu, é uma boa ideia fazer um anúncio público e alertar usuários sobre estas modificações e os meios certificados e seguros de baixar e instalar seu aplicativo oficial.
>
> Também é essencial denunciar os apps maliciosos e solicitar a remoção deles.

Em quais meios as cópias maliciosas do seu app estão sendo distribuídas?

- [no Github](#github)
- [no Gitlab.com](#gitlab)
- [na Google Play Store](#playstore)
- [na Apple App Store](#apple_store)
- [em outro site](#fake_website)

### github

> Se o software malicioso está hospedado no Github, leia [este guia (em inglês)](https://help.github.com/en/articles/guide-to-submitting-a-dmca-takedown-notice) para solicitar remoção de conteúdo que viola direitos autorais.
>
> Pode levar algum tempo para receber a resposta desta solicitação. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#app_end)

### gitlab

> Se o software malicioso está hospedado no GitLab, leia [este guia (em inglês)](https://about.gitlab.com/handbook/dmca/) para solicitar remoção de conteúdo que viola direitos autorais.
>
> Pode levar [algum tempo](https://about.gitlab.com/handbook/support/workflows/services/gitlab_com/dmca.html) para receber a resposta desta solicitação. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#app_end)


### playstore

> Se o software malicioso está hospedado na Google Play, siga [estas instruções](https://support.google.com/legal/troubleshooter/1114905?hl=pt-BR) para solicitar remoção de conteúdo que viola direitos autorais.
>
> Pode levar algum tempo para receber a resposta desta solicitação. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#app_end)



### apple_store

> Se o software malicioso está hospedado na App Store, siga [estas instruções](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts?lang=pt) para solicitar remoção de conteúdo que viola direitos autorais.
>
> Pode levar algum tempo para receber a resposta desta solicitação. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

- [Sim](#resolved_end)
- [Não](#app_end)



### physical_sec_end

> Se você está tendo sua integridade e bem estar físicos ameaçados, por gentileza contate as organizações a seguir que poderão auxiliar melhor nesta situação.

:[](organisations?services=physical_sec)


### account_end

> Se você ainda está tendo suas contas e identidades comprometidas, por gentileza contate as organizações a seguir que poderão apoiar com esta situação.

:[](organisations?services=account&services=legal)


### app_end

> Se o app falso não foi removido, por gentileza contate as organizações a seguir que poderão apoiar com esta situação.

:[](organisations?services=account&services=legal)

### web_protection_end

> Se as solicitações de remoção não foram bem sucedidas, por gentileza contate as organizações a seguir que poderão auxiliar melhor nesta situação.

:[](organisations?services=web_protection)

### secure_comms_end

> Se você precisa de apoio ou recomendações adicionais sobre phishing, segurança de email e criptografia, e comunicações seguras como um todo, você pode entrar em contato com uma das organizações a seguir.

:[](organisations?services=secure_comms)


### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

Para prevenier futuras tentativas de fraude da sua identidade, leia as dicas abaixo.

### final_tips

- Crie senhas fortes: prefira usar uma frase com palavras aleatórias, que não digam respeito a algo pessoal, usando espaços e  caracteres especiais fáceis de lembrar. Mais dicas nos recursos.
- Considere usar um gerenciador de senhas para criar e guardar suas novas senhas, assim você pode manter senhas diferentes para cada site e serviço que usar sem ter que memorizar tudo ou anotar no papel.
- Configure a autenticação em dois fatores em todas as suas contas. A autenticação em dois fatores pode ser muito efetiva para barrar alguém de acessar suas contas sem a sua permissão. Se você puder escolher, não use a autenticação baseada em SMS, prefira usar opções baseadas em aplicativos ou em uma chave de segurança.
- Faça o processo de verificação de conta em suas plataformas de rede social. Algumas plataformas oferecem a possibilidade de verificar sua identidade e sinalizar sua conta de acordo.
- Mapeie sua presença online. Use a inteligência de código aberto disponível na internet para buscar informações disponíveis sobre você e interprete de forma estratégica para conseguir prevenir atores maliciosos de usarem estas informações para se passar por você.
- Configure alertas do Google. Você pode receber emails quando novos resultados para um tópico aparecerem na busca do Google. Por exemplo, você pode receber informação sobre menções do seu nome ou da sua organização/coletivo.
- Guarde um print da sua página como está disponível neste momento como uma evidência futura para casos como este. Se o seu site permite crawlers (robôs de captura de dados), você pode manter uma cópia na Wayback Machine, oferecida pelo archive.org. Visite [este link](https://archive.org/web/), e clique no botão "Save Page Now" ao final da página.

#### resources

- [Criando senhas fortes](https://ssd.eff.org/pt-br/module/criando-senhas-fortes)
- [Animated Overview: Using Password Managers to Stay Safe Online](https://ssd.eff.org/en/module/animated-overview-using-password-managers-stay-safe-online)
- [Choosing a password manager](https://accessnowhelpline.gitlab.io/community-documentation/295-Password_managers.html)
- [Como utilizar o KeePassXC](https://ssd.eff.org/pt-br/module/como-utilizar-o-keepassxc)
- [Two-factor authentication (2FA)](https://www.accessnow.org/need-talk-sms-based-two-step-authentication/)
- [A guide to prevent doxing](https://guides.accessnow.org/self-doxing/self-doxing.html)
- [Archive your website](https://archive.org/web/)
