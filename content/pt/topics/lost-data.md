---
layout: page
title: "Eu perdi meus dados"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: pt
summary: "O que fazer em caso de perda de dados"
date: 2019-03-12
permalink: /pt/topics/lost-data
parent: /pt/
---

# Eu perdi meus dados


Dados digitais podem ser bastante voláteis e instáveis e há muitas formas as quais você pode perdê-los. Danos físicos aos seus aparelhos, encerramento de suas contas digitais, apagamento acidental, atualizações e falhas de software, tudo isso pode proporcionar perda de dados. Além disso, às vezes você pode não estar ciente de como seu sistema de back-up funciona, ou simplesmente ter esquecido as orientações de recuperação - onde encontrar os dados, as senhas etc.

Esta seção do Kit de Primeiros Socorros Digitais irá conduzir você através de passos simples para determinar como você pode ter perdido dados e potenciais estratégias de mitigação e recuperação para eles.

Here is a questionnaire to identify the nature of your problem and find possible solutions.

Siga o questionário para identificar a natureza do problema e achar possívels soluções.

## Workflow

### entry_point

> Nesta seção focaremos em dados de dispositivos. Para conteúdo online e credenciais de acesso, direcionaremos você para outras seções do Kit de Primeiro Socorros Digitais.
>
> Dispositivos incluem computadores, celulares e tablets, HDs externos, pen drives e cartões de memória SD.

Quais tipos de dados você perdeu?

- [Conteúdo Online](../../../account-access-issues)
- [Credenciais](../../../account-access-issues)
- [Conteúdo em dispositivos](#content_on_device)

### content_on_device

> Geralmente, os dados "perdidos" foram simplesmente deletados - seja acidentalmente ou intencionalmente. No seu computador, verifique a Lixeira. Em dispositivos Android, você poderá encontrar alguns dados perdidos na pasta LOST.DIR. Se os arquivos perdidos não puderem ser localizados nas lixeiras do sistema, pode ser que não tenham sido deletados, mas movidos para outro lugar que não seja esperado. Tente usar a função de busca do sistema operacional para localizá-los (a operação pode demorar).
>
> Verifique também arquivos e pastas ocultas, uma vez que os dados podem ter ido parar lá de alguma forma. Veja como fazer isso no [Mac](https://tecnoblog.net/249854/exibir-mostrar-pastas-arquivos-ocultos/) e [Windows](https://support.microsoft.com/pt-br/help/4028316/windows-view-hidden-files-and-folders-in-windows-10). No Linux, vá até o seu diretório /home no seu gerenciador de arquivos e digite Ctrl + H. Esta opção também está disponível em dispositivos Android e iOS.
>
> Tente buscar pelo nome exato do arquivo que você perdeu. Se isto não funcionar, ou se não estiver certa do nome exato, pode tentar uma busca coringa onde você pode buscar por um tipo de arquivo específico. Por exemplo, se perdeu um arquivo do Word mas não sabe o nome, tente buscar `*.docx`, que é a extensão do arquivo. Organize por *Data de Modificação* para localizar mais rapidamente os arquivos mais recentes ao usar este tipo de busca.
>
> Para além de buscar o arquivo perdido em seu dispositivo, pergunte-se se o arquivo foi enviado por e-mail ou compartilhado com alguém (inclusive na sua própria inbox) ou enviado para armazenamento na nuvem em algum momento. Se for o caso, você pode tentar buscar em algum destes lugares e quem sabe recuperar uma cópia ou versão anterior deste arquivo.
>
> Para aumentar as chances de recuperar seus dados, pare o quanto antes de usar seus dispositivos. Continuar gravando arquivos no disco diminui as chances de recuperar estes dados posteriormente. Imagine da mesma forma que tentar recuperar uma mensagem em um bloco de notas - quanto mais você escreve por cima, mais difícil fica rabiscar o que tinha antes para saber o que estava escrito.

Como você perdeu seus dados?

- [O dispositivo onde os dados estavam sofreu dano físico](#tech_assistance_end)
- [O dispositivo onde os dados estavam foi roubado/perdido](#device_lost_theft_end)
- [Os dados foram deletados](#where_is_data)
- [Os dados sumiram depois de uma atualização de sistema](#software_update)
- [O sitema ou software parou de funcionar e depois os dados sumiram](#where_is_data)


### software_update

> Às vezes, quando você atualiza um software ou o sistema operacional, problemas inesperados podem acontecer, fazendo com que este software ou sistema parem de funcionar como deveriam, ou comportamentos defeituosos passem a acontecer, incluindo dados corrompidos. Se você notar uma perda de dados logo após uma atualização de software ou de sistema, é importante considerar regredir para o estágio anterior.
>
> É comum que sistemas baseados em software possuam meios de fazer isso, e este procedimento é chamado de rollback. Rollbacks são úteis porque significam que um software ou sistema e sua base de dados pode ser restaurada para um estado consistente antes que falhas tenham as afetado. A facilidade ou dificuldade em executar estes procedimentos depende de como este software foi construído - em alguns casos é mais simples e em outros nem tanto, e em alguns casos requer trabalho mais pesado. Você irá precisar fazer uma busca online para saber como voltar para versões mais antigas de determinados programas. Outro termo para "rollback" que você irá encontrar é "downgrading", então busque por esta alternativa também. Para sistemas operacionais como Windows ou Mac, existem métodos específicos para retornar à versões mais antigas.
>
> - Para sistemas Mac, visite a [base de conhecimento da Central de Suporte Mac](https://support.apple.com) e use a palavra-chave "downgrade" com a sua versão de macOS para ler mais e encontrar instruções.
> - Para sistemas Windows, o procedimento varia dependendo se você quer desfazer um update específico ou fazer o rollback do sistema inteiro. Em ambos os casos, você pode encontrar instruções na [Central de Suporte Microsoft](https://support.microsoft.com), por exemplo para o Windows 10 você pode procurar por "como remover uma atualização instalada" nestas [Perguntas Frequentes](https://support.microsoft.com/pt-br/help/12373/windows-update-faq).

O rollback do software ou sistema foi útil?

- [Sim](#resolved_end)
- [Não](#where_is_data)


### where_is_data

> Fazer backup regularmente - a prática de guardar seus dados importantes em pelo menos outros dois dispositivos - é ótimo e altamente recomendado. Às vezes nos esquecemos que temos backups automáticos em alguns casos, então vale a pena verificar seus dispositivos para saber se você tem essa opção habilitada, e usar este backup para restaurar seus dados. Caso não tenha, planejar backups futures é recomendado, e você deve encontrar mais dicas sobre como configurá-los nas [dicas finais](#resolved_end).

Para verificar se você tem um backup dos dados perdidos, comece se perguntando onde estes dados ficavam armazenados.

- [Em dispositivos de armazenamento externo (HDs, pen drives, cartões SD)](#storage_devices_end)
- [Em um computador](#computer)
- [Em um dispositivo móvel](#mobile)

### computer

Qual sistema operacional você tem em seu computador?

- [MacOS](#macos_computer)
- [Windows](#windows_computer)
- [Gnu/Linux](#linux_computer)

### macos_computer

> Para verificar se o seu dispositivo MacOS tem uma opção de backup ligada, e utilizá-la para restaurar seus dados, verifique sua [iCloud](https://support.apple.com/pt-br/HT208682) ou [Time Machine](https://support.apple.com/pt-br/HT201250) para ver se há backups automáticos disponíveis para os seus dados.
>
> Um lugar para verificar geralmente é a sua lista de Itens Recentes, que mantém registro dos aplicativos, arquivos e servidores que você usou durante as suas últimas sessões no computador. Para buscar por arquivos e reabrí-los, vá ao menu Apple no canto superior esquerdo (ícone da maçã), selecione Itens Recentes e navegue através da lista de arquivos. Mais detalhes podem ser encontrados [aqui](https://pcworld.com.br/acesse-com-rapidez-itens-recentes-no-mac/).

Você conseguiu localizar ou restaurar seus dados?

- [Sim](#resolved_end)
- [Não](#tech_assistance_end)

### windows_computer

> Para verificar se você possui o backup ligado em seu dispositivo Windows, pode ler [estas instruções](https://support.microsoft.com/pt-br/help/4027408/windows-10-backup-and-restore).
>
> O Windows 10 inclui a função **Timeline** que se propõe a aumentar a produtividade guardando registros dos arquivos que você usa, sites que você navega e outras ações que você executa no seu computador. Se você não consegue lembrar onde guardou um documento, você pode clicar no ícone da Timeline na barra de tarefas do Windows 10 e ver um diário visual organizado por data, e pular para aquilo que você precisa clicando no ícone de visualização apropriado. Isto pode te ajudar a localizar um arquivo que foi renomeado. Se a Timeline estiver habilitada, algumas atividades no seu PC - como arquivos que você editou no Microsoft Office - também podem estar sincronizadas com seu dispositivo móvel ou outro computador que você utilize, então você talvez tenha um backup destes dados perdidos em outro dispositivo caso estejam conectados ao seu PC. Você pode ler mais sobre esta funcionalidade e como utilizá-la [aqui](https://www.techtudo.com.br/dicas-e-tutoriais/2018/01/como-usar-o-recurso-timeline-do-windows-10.ghtml).

Você conseguiu localizar ou restaurar seus dados?


- [Sim](#resolved_end)
- [Não](#windows_restore)

### windows_restore

> Em certos casos há ferramentas de código aberto que podem ajudar a encontrar conteúdo perdido e recuperá-lo. Algumas vezes o uso delas será limitado, e a maioria das ferramentas mais reconhecidas custará algum dinheiro.
>
> Por exemplo, o [Recuva](http://www.ccleaner.com/recuva ) é um software gratuito bastante conhecido para recuperação de dados. Recomendamos tentar e ver como funciona para o seu caso.

Esta informação foi útil para recuperar seus dados (parcial ou completamente)?

- [Sim](#resolved_end)
- [Não](#tech_assistance_end)


### linux_computer

> Algumas das distribuições de Linux mais populares, como o Ubuntu, possuem ferramentas de backup já integradas, como por exemplo a [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup). Se uma ferramenta de backup estiver integrada no sistema operacional, a configuração automática de backup pode ter sido solicitada ao iniciar o computador pela primeira vez. Procure saber se sua distribuição do Linux possui ferramenta de backup integrado, e qual o procedimento para verificar se ela está habilitada e restaurar seus dados.
>
> No caso do Déja Dup para o Ubuntu, você pode dar uma olhada [neste artigo](https://www.dedoimedo.com/computers/ubuntu-deja-dup.html) para chegar se você possui backups automáticos habilitados, e [esta página](https://wiki.gnome.org/DejaDup/Help/Restore/Full) para instruções sobre como recuperar dados perdidos de um backup existente.

Esta informação foi útil para recuperar seus dados (parcial ou completamente)?

- [Sim](#resolved_end)
- [Não](#tech_assistance_end)

### mobile

Qual o sitema operacional do seu telefone?

- [iOS](#ios_phone)
- [Android](#android_phone)


### ios_phone

> Você pode ter um backup automático conectado com seu iCloud ou iTunes. Leia [este guia](https://support.apple.com/kb/PH12521?viewlocale=pt_BR) para verificar se você tem backups disponíveis e como restaurá-los.

Você encontrou seu backup e recuperou seus dados?

- [Sim](#resolved_end)
- [Não](#phone_which_data)

###  phone_which_data

Qual das situações se aplica à perda dos seus dados?

- [Foram dados gerados por aplicativos (contatos, chats etc.)](#app_data_phone)
- [Foram dados gerados por mim (fotos, vídeos, áudios, notas etc.)](#ugd_phone)

### app_data_phone

> Um aplicativo mobile é uma aplicação de software feita para funcionar no seu dispositivo móvel (celular ou tablet). No entanto, a maior parte destes aplicativos também pode ser acessada através de um navegador de internet em seu computador. Se você sofrer uma perda de dados de aplicativo no seu celular, tente acessar o aplicativo em seu computador a partir de sua interface web (se disponível) com suas credenciais. Pode ser que seus dados estejam disponíveis através desta interface.

Esta informação foi útil para recuperar seus dados (parcial ou completamente)?

- [Sim](#resolved_end)
- [Não](#tech_assistance_end)

### ugd_phone

> Dados gerados por usuário são aqueles que você gera durante a utilização de certos aplicativos, e em caso de perda de dados você deve verificar se este aplicativo possui configurações de backup ativadas por padrão ou habilita formas de recuperá-lo. Por exemplo, se você usa o WhatsApp no seu celular e perdeu o histórico de conversas, ou um problema com o aparelho aconteceu, você pode recuperar as conversas caso tenha habilitado as configurações de recuperação do Whatsapp. Ou se estiver usando um app para criar notas com conteúdo sensível e informações pessoais por exemplo, provavelmente terá uma opção habilitada de backup que pode não ter sido percebida durante o uso diário.

Esta informação foi útil para recuperar seus dados (parcial ou completamente)?

- [Sim](#resolved_end)
- [Não](#tech_assistance_end)

### android_phone

> O Google tem um serviço integrado no Android, chamado Backup Service. Por padrão, este serviço guarda diversos tipos de dados e associa com o serviço do Google apropriado, que você pode também acessar pela web. Você pode olhar suas configurações de sincronização indo em Configurações > Contas > Google, e então selecionando sua conta do Gmail. Se você perder dados de alguma categoria que esteja sincronizada com sua conta Google, provavelmente terá a possibilidade de recuperá-los acessando sua conta Google através do navegador web.

Esta informação foi útil para recuperar seus dados (parcial ou completamente)?

- [Sim](#resolved_end)
- [Não](#phone_which_data)


### tech_assistance_end

> Se sua perda de dados for causada por danos físicos como uma queda do dispositivo no chão ou na água, falhas de eletricidade, ou outros problemas, o mais provável é que você precisará executar uma recuperação dos dados em seus discos rígidos. Se você não sabe como fazer isso, contate uma pessoa com conhecimentos de tecnologias nas áreas de manutenção de hardware e equipamentos eletrônicos para lhe auxiliar. No entanto, dependendo do contexto e da confidencialidade dos dados que precise recuperar, não aconselhamos que procure lojas de informática desconhecidas e sim que busque por indicações de pessoas e organizações de confiança e com consciência sobre privacidade e consentimento.

### device_lost_theft_end

> Em caso de dispositivos perdidos ou roubados, certifique-se de mudar todas as suas senhas assim que possível e visitar o nosso recurso sobre [o que fazer quando perder um dispositivo](../../../I lost my devices).
>
> Se você é um membro da sociedade civil e precisa de apoio para adquirir um novo dispositivo, entre em contato com uma das organizações listadas abaixo.

:[](organisations?services=equipment_replacement)


### storage_devices_end

> Para recuperar os dados que você perdeu, lembre-se que a noção de tempo é essencial. Por exemplo, recuperar um arquivo deletado acidentalmente há poucas horas atrás ou no dia anterior terá maior chance de sucesso do que um arquivo deletado há meses.
>
> Considere usar uma ferramenta de recuperação para casos de perda ou deleção, como o [Recuva para Windows](https://www.ccleaner.com/recuva/download), e para Gnu/Linux você pode investigar as alternativas disponíveis para sua distribuição. Leve em conta que estas ferramentas nem sempre vão funcionar, porque seu sistema operacional pode ter colocado dados novos por cima da informação apagada, por exemplo. Por causa disso, até tentar restaurar os dados com ferramentas como estas, use o computador o menos possível.
>
> Para aumentar suas chances de recuperar dados, pare de usar seus dispositivos imediatamente. Continuar gravando no disco rígido irá diminuir as chances de encontrar e recuperar dados.
>
> Se nenhuma das opções acima funcionar para você, o mais provável é que você precisará executar uma recuperação dos dados em seus discos rígidos. Se você não sabe como fazer isso, contate uma pessoa com conhecimentos de tecnologias nas áreas de manutenção de hardware e equipamentos eletrônicos para lhe auxiliar. No entanto, dependendo do contexto e da confidencialidade dos dados que precise recuperar, não aconselhamos que procure lojas de informática desconhecidas e sim que busque por indicações de pessoas e organizações de confiança e com consciência sobre privacidade e consentimento.


### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Backups - Além das dicas citadas acima, é sempre uma boa ideia ter a certeza de ter feito backups - muitos backups, sempre em outros lugares diferentes de onde seus dados estão! Dependendo do contexto, opte por guardar seus dados em servições de armazenamento na nuvem e também em dispositivos de armazenamento físicos sem contato com a internet.
- Para ambos os tipos de backups, é importante proteger seus dados com criptografia. Mantenha backups regulares e incrementais (que adicionam novos arquivos e versões novas de antigos arquivos) para seus dados mais importantes, e verifique que você tenha estes backups feitos antes de fazer atualizações de sistema e softwares importantes.
- Crie uma estrutura de arquivos organizada - Cada pessoa tem o seu jeito de organizar dados e informação e não existe a melhor forma a não ser a sua. No entando, é importante considerar criar um sistema de pastas que atenda suas necessidades. Ao criar um sistema de pastas consistente, você torna sua vida mais fácil sabendo melhor quais pastas e arquivos devem ter o backup feito constantemente, onde estão as informações mais importantes, onde você deve manter os dados mais sensíveis e pessoais sobre você e pessoas envolvidas com o que você atua, e por aí vai. Como de costume, pare um pouquinho, respire fundo, olhe pra sua tela e leve seu tempo planejando e entendendo o tipo de dado que você produz e trabalha no seu dia, e pense em um sistema de arquivos que torne seu processo criativo e produtivo organizado e consistente. Se você não sabe por onde começar, [este guia](https://blog.trello.com/br/como-organizar-arquivos-e-pastas) pode ajudar a ter uma ideia inicial.


#### resources

- [Security in a Box - Táticas de Backup](https://securityinabox.org/pt/guide/backup/)
- [Páginna Oficial sobre Backup no Mac (em inglês)](https://support.apple.com/mac-backup)
- [Como fazer backup regularmente no Windows 10](https://support.microsoft.com/pt-br/help/4027408/windows-10-backup-and-restore)
- [Como habilitar backup automático no iPhone](https://support.apple.com/pt-br/HT203977)
- [Como habilitar backup automático no Android](https://support.google.com/android/answer/2819582?hl=pt-BR).
